package ru.gdc.action;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConvertToHtmlTest {
    @Test
    public void convert() throws Exception {
        String text = "[i]курсив[/i] [b]жирный текст[/b] [url=http://icl-services.com]ICL Services[/url]";
        String convertedText = "<i>курсив</i> <b>жирный текст</b> <a href=http://icl-services.com>ICL Services</a>";

        assertEquals(convertedText, ConvertToHtml.convert(text));
    }

}