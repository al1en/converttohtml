package ru.gdc.action;


/**
 *Класс служит для конверторования текста формата BBCode в формат Html
 */
public class ConvertToHtml {

    /**
     * Основная функция конвертирования
     * return String
     */
    public static String convert(String text) {
        text = text.replace('[', '<');
        text = text.replace(']', '>');
        text = text.replaceAll("url=", "a href=");
        text = text.replaceAll("/url", "/a");

        return text;
    }
}
