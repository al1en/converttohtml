package ru.gdc.servlets;

import ru.gdc.action.ConvertToHtml;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Оснавной класс отвечающий за GET и POST запросы
 */
@WebServlet("/main")
public class MyServlet extends HttpServlet {
    private static final Logger logger  = Logger.getLogger(MyServlet.class.getName());


    /**
     * Отвечает на GET запрос, переводя текст формата BBCode, в формат Html и отправляет обратно клиенту
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Пришел GET запрос от клиента");

        RequestDispatcher dispatcher = request.getRequestDispatcher("/hello.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Отвечает на POST запрос, переводя текст формата BBCode, в формат Html и отправляет обратно клиенту
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Пришел POST запрос от клиента");
        request.setCharacterEncoding("UTF-8");

        //получаем введеный строку в текст боксе
        String text = request.getParameter("hyperText");
        logger.info("Получили текст от клиента");

        response.setContentType("text/html;charset=UTF-8");

        //отправляем клиенту текст, с тегами Html
        response.getWriter().println("<!DOCTYPE HTML>");
        response.getWriter().println(ConvertToHtml.convert(text));
        logger.info("Html текст отправлен в ответ");

    }
}
